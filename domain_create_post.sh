#!/bin/sh

F=/etc/virtual/$domain/filter.conf
if [ -e $F ]; then
   echo "high_score=20" > $F
   echo "high_score_block=yes" >> $F
   echo "where=userspamfolder" >> $F
   echo "action=rewrite&value=filter&user=$username" >> /usr/local/directadmin/data/task.queue
fi