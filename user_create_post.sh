#!/bin/sh
if [ "$spam" = "ON" ]; then
  DIR=/home/$username/.spamassassin
  mkdir -p $DIR
  UP=$DIR/user_prefs
  if [ ! -s ${UP} ]; then
     echo 'required_score 5.0' > ${UP}
     echo 'report_safe 1' >> ${UP}
     chown $username:$username  ${UP}
     chmod 644 ${UP}
  fi
  chown  ${username}:mail $DIR
  chmod 771 $DIR

  if [ "${domain}" != "" ]; then
     FCONF=/etc/virtual/${domain}/filter.conf
     if [ ! -s ${FCONF} ]; then
        echo 'high_score=20' > ${FCONF}
        echo 'high_score_block=yes' >> ${FCONF}
        echo 'where=userspamfolder' >> ${FCONF}
        chown mail:mail ${FCONF}

        echo "action=rewrite&value=filter&user=$username" >> /usr/local/directadmin/data/task.queue
     fi
  fi
fi
exit 0;